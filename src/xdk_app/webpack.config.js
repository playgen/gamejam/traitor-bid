var path = require('path');

module.exports = {
	entry: "./core/app.js",
	output: {
		filename: "core_bundle.js"
	},
	resolve: {
		modules: [
			'./Common',
			'./Core',
		]
	},	
	module: {
		rules: [
			{ 
				test: /\.js$/, 
				exclude: /(node_modules|bower_components)/,
				loader: "babel-loader"
			}
		]
	},
	devtool: 'source-map'
};