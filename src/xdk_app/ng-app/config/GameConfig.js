angular
	.module("traitorBid")
	.value("GameConfig", {
		MinPlayers: 3,
		MaxPlayers: 6,
		TraitorCount: 1,
		
		MissionCount: 3,
		MissionMinResourceTypes: 2,
		MissionMaxResourceTypes: 3,
		MissionMinQuantity: 2,
		MissionMaxQuantity: 5,

		ResourceTypes: [
			"Actuator",
			"Battery",
			"Chip",
			"Cog",
			"Motor",
		],
		ResourcesPerPlayer: [5, 3, 2],
	});