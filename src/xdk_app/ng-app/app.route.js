angular
	.module("traitorBid")
	.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
		$stateProvider
			// .state("game", {
			// 	abstract: true,
			// })
			.state("home", {
				url: "/",
				component: "pageHome"
			})
			.state("playerSelect", {
				component: "pagePlayerSelect"
			})
			.state("players", {
				component: "pagePlayers"
			})
			.state("mission", {
				component: "pageMission",
				onEnter: ["$transition$", function($transition$) {
					$transition$.injector().get("GameService").Game.NextMission();
				}],
			})
			.state("playerLock", {
				component: "pagePlayerLock",
				onEnter: ["$transition$", function($transition$) {
					$transition$.injector().get("GameService").Game.NextPlayer();
				}],
			})
			.state("playerPrivate", {
				component: "pagePlayerPrivate",
			})
			.state("postBid", {
				component: "pagePostBid",
			})
			.state("missionResult", {
				component: "pageMissionResult",
			})
			;
			$urlRouterProvider.otherwise("/");
	}]);