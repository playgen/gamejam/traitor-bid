angular
	.module("traitorBid")
	.component("resource", {
		templateUrl: "components/resource/resource.html",
		bindings: {			
			resource: "<",
			showQuantity: "<",
		},
		controller: [
			"$state",
			function($state) 
			{
				var ctrl = this;

				ctrl.sprite = ctrl.resource.Type.toLowerCase();
			}
		]
	});