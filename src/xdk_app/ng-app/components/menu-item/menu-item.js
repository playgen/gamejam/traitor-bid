angular
	.module("traitorBid")
	.component("menuItem", {
		templateUrl: "components/menu-item/menu-item.html",
		bindings: {			
			item: "<"
		},
		controller: [
			"$state",
			function($state) 
			{
				var ctrl = this;

				if (ctrl.item.hasOwnProperty("enabled") === false) {
					ctrl.item.enabled = function() { return true; };
				}
			}
		]
	});