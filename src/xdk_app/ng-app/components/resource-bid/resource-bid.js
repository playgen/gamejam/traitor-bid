angular
	.module("traitorBid")
	.component("resourceBid", {
		templateUrl: "components/resource-bid/resource-bid.html",
		bindings: {
			resources: "<",
		},
		controller: [
			"GameService",
			function(GameService) 
			{
				var ctrl = this;

				ctrl.bid = [];

				ctrl.incrementBid = function(resource) {
					var bids = ctrl.bid.filter(function(r) { 
						return r.Type == resource.Type; 
					});
					if (bids.length == 1) {
						var bid = bids[0];
						if (resource.Quantity > 0) {
							bid.Quantity += 1;
							resource.Quantity -= 1;
						}
					}
					else {
						resource.Quantity -= 1;
						ctrl.bid.push(new Resource(resource.Type, 1, 0));
					}
				};
				
				ctrl.decrementBid = function(bid) {
					var resources = ctrl.resources.filter(function(r) { 
						return r.Type == bid.Type; 
					});
					if (resources.length == 1) {
						var resource = resources[0];
						if (bid.Quantity > 0) {
							resource.Quantity += 1;
							bid.Quantity -= 1;
						}
					}

					ctrl.bid = ctrl.bid.filter(function(r) { return r.Quantity > 0; });
				};
			}
		]
	});