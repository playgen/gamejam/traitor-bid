angular
	.module("traitorBid")
	.component("pageHome", {
		templateUrl: "pages/home/page-home.html",
		controller: [
			"$state",
			"AppConfig",
			function($state, AppConfig) 
			{
				var ctrl = this;

				ctrl.header = AppConfig.Title;

				ctrl.$onInit = function () {
					console.log("pageHome controller onInit");
				};

				ctrl.menuItems = [
					{
						label: "NEW GAME",
						click: function () {
							console.log("pageHome menu: " + this.label);
							$state.go('playerSelect');
						}
					}
				];
			}
		]
	});