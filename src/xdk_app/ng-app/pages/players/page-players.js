angular
	.module("traitorBid")
	.component("pagePlayers", {
		templateUrl: "pages/players/page-players.html",
		controller: [
			"$state",
			"GameService",
			function($state, GameService) 
			{
				var ctrl = this;

				ctrl.$onInit = function () {
					console.log("pageHome controller onInit");

					ctrl.players = GameService.Game.Players;
					setHeader();
				};

				ctrl.allSelected = function() {
					return ctrl.players.reduce(function(acc, player, i) { return acc && player.Index > -1; }, true);
				};

				ctrl.footerMenuItems = [
					{
						label: "CONTINUE",
						click: function() {
							if (this.enabled()) {
								GameService.Game.Start();
								$state.go("mission");
							}
						},
						enabled: function() {
							return ctrl.allSelected();
						}
					}
				];


				var currentPlayerIndex = 0;
				ctrl.selectPlayer = function(player) {
					if (player.Index == -1)
					{
						player.Index = currentPlayerIndex;
						currentPlayerIndex++;
						setHeader();
					}
				};

				function setHeader() {
					if (ctrl.allSelected()) {
						ctrl.header = "READY TO PLAY";
					}
					else {
						ctrl.header = "PLAYER " + (currentPlayerIndex + 1) + " PICK A ROBOT"; 
					}
				}
			}
		]
	});