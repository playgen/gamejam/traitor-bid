angular
	.module("traitorBid")
	.component("pagePostBid", {
		templateUrl: "pages/postBid/page-postBid.html",
		controller: [
			"$state",
			"$interval",
			"GameService",
			function($state, $interval, GameService) 
			{
				var ctrl = this;

				var truthInterval = null;

				ctrl.truth = (GameService.Game.DeceptionLevel * 100).toString().substr(0,5);

				ctrl.$onInit = function() {
					truthInterval = $interval(function() {
						ctrl.truth = (GameService.Game.DeceptionLevel * 100).toString().substr(0,5);
					}, 1000);
				};

				ctrl.$onDestroy = function() {
					$interval.cancel(truthInterval);
				};

				ctrl.Mission = GameService.Game.CurrentMission;

				ctrl.footerMenuItems = [
				{
					label: "NEXT PLAYER",
					click: function() {
						if (GameService.Game.CurrentBiddingComplete) {
							$state.go("result");
						}
						else {
							$state.go("playerLock");
						}
					},
					enabled: function() { return true; }
				}];
			}
		]
	});