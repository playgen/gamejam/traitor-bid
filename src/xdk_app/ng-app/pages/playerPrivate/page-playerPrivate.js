angular
	.module("traitorBid")
	.component("pagePlayerPrivate", {
		templateUrl: "pages/playerPrivate/page-playerPrivate.html",
		controller: [
			"$state",
			"GameService",
			function($state, GameService) 
			{
				var ctrl = this;

				ctrl.Player = GameService.Game.CurrentPlayer;
				ctrl.Mission = GameService.Game.CurrentMission;

				ctrl.footerMenuItems = [
					{
						label: "END TURN",
						click: function() {
							GameService.Game.ProcessBid();
							$state.go("postBid");
						},
						enabled: function() { return true; }
					}];

			}
		]
	});