angular
	.module("traitorBid")
	.component("pagePlayerSelect", {
		templateUrl: "pages/playerSelect/page-playerSelect.html",
		controller: [
			"$state",
			"GameConfig",
			"GameService",
			function($state, GameConfig, GameService) 
			{
				var ctrl = this;

				ctrl.$onInit = function () {
					console.log("pagePlayerSelect controller onInit");
				};

				ctrl.header = "NUMBER OF PLAYERS";

				function selectPlayerCount(count) {
					GameService.Initialize(count);
					$state.go('players');
				}

				ctrl.menuItems = [];
				
				for(var i = GameConfig.MinPlayers; i <= GameConfig.MaxPlayers; i++) {
					var pc = i;
					ctrl.menuItems.push({
						label: i + " PLAYERS",
						value: i,
						click: function() { 
							selectPlayerCount(this.value); 
						}
					});
				}

			}
		]
	});