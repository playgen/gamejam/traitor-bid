angular
	.module("traitorBid")
	.component("pagePlayerLock", {
		templateUrl: "pages/playerLock/page-playerLock.html",
		controller: [
			"$state",
			"GameService",
			function($state, GameService) 
			{
				var ctrl = this;

				ctrl.unlock = function() {
					$state.go("playerPrivate");
				};

				ctrl.Player = GameService.Game.CurrentPlayer;
			}
		]
	});