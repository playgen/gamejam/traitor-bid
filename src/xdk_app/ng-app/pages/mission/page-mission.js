angular
	.module("traitorBid")
	.component("pageMission", {
		templateUrl: "pages/mission/page-mission.html",
		controller: [
			"$state",
			"GameService",
			function($state, GameService) 
			{
				var ctrl = this;

				ctrl.Mission = GameService.Game.CurrentMission;

				ctrl.footerMenuItems = [
				{
					label: "BEGIN",
					click: function() {
						$state.go("playerLock");
					},
					enabled: function() { return true; }
				}];

			}
		]
	});