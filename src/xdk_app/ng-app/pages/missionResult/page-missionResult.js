angular
	.module("traitorBid")
	.component("pageMissionResult", {
		templateUrl: "pages/missionResult/page-missionResult.html",
		controller: [
			"$state",
			"GameService",
			function($state, GameService) 
			{
				var ctrl = this;

				ctrl.Mission = GameService.Game.CurrentMission;
			}
		]
	});