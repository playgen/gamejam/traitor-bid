angular
	.module("traitorBid")
	.factory("GameService", 
		[
			"GameConfig",
			function(gameConfig) {

				var service = {};

				service.Game = {};

				service.Initialize = function(numPlayers) {
					service.Game = new Game(numPlayers, gameConfig);
				};

				return service;
			}
		]);