import * as Assert from 'Assert';

class LogLevel {
	constructor() {
		
	}
	static get Debug() { return Symbol.for('LogLevel.Debug'); }
	static get Info() { return Symbol.for('LogLevel.Info'); }
	static get Warning() { return Symbol.for('LogLevel.Warning'); }
	static get Error() { return Symbol.for('LogLevel.Error'); }
}

class Logger {
	constructor(logLevel) {
		this.LogLevel = logLevel;
	}

	Debug(message) {
		this.Log(message, LogLevel.Debug);
	}

	Info(message) {
		this.Log(message, LogLevel.Info);
	}

	Warning(message) {
		this.Log(message, LogLevel.Warning);
	}
	Error(message) {
		this.Log(message, LogLevel.Error);
	}
}

class ConsoleLogger extends Logger {
	constructor(logLevel) {
		super(logLevel);
	}

	Log(message, level) {
		switch(level) {
			case LogLevel.Debug:
				console.debug(message);
				break;
			case LogLevel.Info:
				console.log(message);
				break;
			case LogLevel.Warning:
				console.warn(message);
				break;
			case LogLevel.Error:
				console.error(message);
				break;
		}
	}

}

export { LogLevel, Logger, ConsoleLogger };