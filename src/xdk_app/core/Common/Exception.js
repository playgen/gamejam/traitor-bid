class Exception extends Error {
	constructor(message) {
		super(message);
		this.Message = message;
	}
}

export default Exception;