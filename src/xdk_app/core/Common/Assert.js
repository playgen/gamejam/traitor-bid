import Exception from 'Exception';
import * as Test from 'Test';

export function NotNull(ref) {
	if (ref === undefined) {
		throw new Exception("Parameter cannot be null.");
	}
}

export function IsArray(ref) {
	if (Array.isArray(ref) === false) {
		throw new Exception("Parameter must be an array.");
	}
}

export function IsType(ref, type) {
	if (Test.IsType(ref, type) === false) {
		throw new Exception("Object is not of correct type.");
	}
}

export function IsMember(ref, declaringType) {
	if (Test.IsMember(ref, declaringType) === false) {
		throw new Exception("Object is not a member of specified type.");
	}
}

export function IsTrue(value) {
	if (value === false) {
		throw new Exception("Value is not true");
	}
}