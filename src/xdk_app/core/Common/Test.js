export function IsType(ref, type) {
	return ref instanceof type;
}

// test if the declaring type contains a member with the corresponding value
// (enum member checking)
export function IsMember(ref, declaringType) {
	return Reflect.ownKeys(declaringType)
		.filter(k => typeof declaringType[k] == typeof Symbol())
		.map(k => declaringType[k])
		.includes(ref);
}