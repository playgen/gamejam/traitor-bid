import Role from 'Role';
import Robot from 'Robot';
import Player from 'Player';
import Resource from 'Resource';
import Mission from 'Mission';
import DeceptionProcessor from 'DeceptionProcessor';

import * as Assert from 'Assert';

class Game {
	constructor(playerCount, gameConfig) {

		this._players = this.GeneratePlayers(playerCount, gameConfig);

		this._currentPlayerIndex = -1;

		this._resources = [];

		this._missions = this.GenerateMissions(playerCount, gameConfig);

		this._currentMissionIndex = -1;

		this._currentBiddingComplete = false;
	}

	get Players() {
		return this._players || [];
	}

	get CurrentPlayer() {
		return this._currentPlayerIndex == -1 
			? null 
			: this._players[this._currentPlayerIndex];
	}

	get CurrentMission() {
		return this._currentMissionIndex == -1
			? null
			: this._missions[this._currentMissionIndex];
	}

	get DeceptionLevel() {
		return new DeceptionProcessor().GetTruth();
	}

	get CurrentBiddingComplete() {
		return this._currentBiddingComplete;
	}

	ProcessBid() {
		var currentPlayerBid = this.CurrentPlayer.Bid;
		for(var i = 0; i < currentPlayerBid.length; i++) {
			this.CurrentMission.Resources.push(currentPlayerBid[i]);
		}
		currentPlayerBid = [];
	}

	GenerateMissions(playerCount, gameConfig) {
		var missions = [];
		for(var i = 0; i < gameConfig.MissionCount; i++) {
			var numResources = gameConfig.MissionMinResourceTypes + Math.round(Math.random() * (gameConfig.MissionMaxResourceTypes - gameConfig.MissionMinResourceTypes));
			var resourceTypes = gameConfig.ResourceTypes.slice().shuffle().slice(0,numResources);
			var resources = resourceTypes.map(function(rt) { return new Resource(rt, 0, 0); });
			var quantity = (playerCount * gameConfig.MissionMinQuantity) + Math.round(Math.random() * (gameConfig.MissionMaxQuantity - gameConfig.MissionMinQuantity));
			var mission = new Mission(resources, quantity);
			missions.push(mission);
		}
		return missions;
	}

	
	GeneratePlayers(playerCount, gameConfig) {
		var numTraitors = gameConfig.TraitorCount;
		
		function GetRole(i) {
			var traitor = numTraitors > 0 && Math.random() > 0.5;
			if (traitor)
			{
				numTraitors--;
			}
			return new Role(traitor);
		}

		var robots = Robot.Sprites.slice();

		function GetRobot() {
			var robotIndex = Math.floor(robots.length * Math.random());
			var robot = new Robot(robots[robotIndex]);
			robots.splice(robotIndex, 1);
			return robot;
		}

		var players = [];

		for (var i = 0; i < playerCount; i++) {
			var player = new Player(GetRole(i), GetRobot());
			players.push(player);
		}

		function AllocateResources(players) {
			// shuffle the resources
			gameConfig.ResourceTypes.slice().shuffle();
			// generate the initial pool
			for(var p = 0; p < playerCount; p++) 
			{
				var player = players[p];
				for(var i = 0; i < gameConfig.ResourcesPerPlayer.length; i++) {
					var resourceType = gameConfig.ResourceTypes[(p + i) % gameConfig.ResourceTypes.length];
					var resourceQuantity = gameConfig.ResourcesPerPlayer[i];
					console.log("allocating resource " + resourceType + "[" + resourceQuantity + "] to player: " + p);
					player.Resources.push(new Resource(resourceType, resourceQuantity, resourceQuantity));
				}
			}
		}

		AllocateResources(players);

		return players;
	}

	Start() {
		this._players.sort(function(a, b) { 
			return a.Index < b.Index
				? -1
				: a.Index > b.Index
					? 1
					: 0;
		});
	}

	NextMission() {
		this._currentMissionIndex = (this._currentMissionIndex + 1) % this._missions.length;
	}

	NextPlayer() {
		this._currentPlayerIndex += 1;
		if (this._currentPlayerIndex == this._players.length) {
			this._currentBiddingComplete = true;
			this.CurrentMission.Aggregate();
		}
	}
}

export default Game;