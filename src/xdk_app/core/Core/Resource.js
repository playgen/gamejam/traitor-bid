class Resource { 
	constructor(type, quantity, quantityPerTurn){
		this._type = type;
		this._quantity = quantity || 0;
		this._quantityPerTurn = quantityPerTurn || 0;
	}

	get Type() {
		return this._type;
	}

	get Quantity() {
		return this._quantity;
	}
	set Quantity(value) {
		this._quantity = value;
	}

	get QuantityPerTurn() {
		return this._quantityPerTurn;
	}
	set QuantityPerTurn(value) {
		this._quantityPerTurn = value;
	}

	Generate() {
		this._quantity += this._quantityPerTurn;
	}
}

export default Resource;