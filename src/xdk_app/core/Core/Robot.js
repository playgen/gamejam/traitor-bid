class Robot {

	static get Sprites() {
		return [
			"robot_01",
			"robot_02",
			"robot_03",
			"robot_04",
			"robot_05",
			"robot_06",
		];
	}

	constructor(sprite) {
		this.Sprite = sprite;
	}
}

export default Robot;