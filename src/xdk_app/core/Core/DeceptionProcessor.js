
class DeceptionProcessor {
	constructor() {
		this._mu = 0.5;
		this._theta = 1;
		this._rootTwoPi = Math.sqrt(2 * Math.PI);
	}

	GetTruth() {
		var seed = Math.random();
		var truth = this.GaussianTruth(seed);
		return truth;
	}

	GaussianTruth(x) {
		var alpha = -1*x*x*0.5;
		return Math.pow(Math.E, alpha) / this._rootTwoPi;
	}
}

export default DeceptionProcessor;