class Mission {
	constructor(requiredResources, requiredQuantity) {
		this._requirements = {
			Resources: requiredResources,
			Quantity: requiredQuantity,
			ResourceKeys: requiredResources.map(function(r) { return r.Type; }),
		};
		this._resources = [];
	}

	get Requirements() {
		return this._requirements;
	}

	get Resources() {
		return this._resources;
	}

	get ResourceCount() {
		return this._resources.length;
	}

	AddResource(resource) {
		this._resources.push(resource);
	}

	get CheckResult() {
		var resources = this._resources.reduce(function(acc, r, i) { 
			if (acc[r.Type] == undefined) { 
				acc[r.Type] = r.Quantity; 
			} 
			else { 
				acc[r.Type] += r.Quantity; 
			} 
			return acc; 
		}, {});
		
		var q = 0;
		
		var resourceKeys = Object.getOwnPropertyNames(resources);
		for(var i = 0; i < resourceKeys.length; i++)
		{
			if (this._requirements.ResourceKeys.indexOf(resourceKeys[i]) >= 0) {
				q += resources[resourceKeys[i]];
			}
			else {
				q -= resources[resourceKeys[i]];
			}
		}
		return {
			Quantity: q,
			IsSatisfied: q >= this._requirements.Quantity,
		};
	}

	Aggregate() {
		var resources = this._resources.reduce(function(acc, r, i) { 
			if (acc[r.Type] == undefined) { 
				acc[r.Type] = r; 
			} 
			else { 
				acc[r.Type].Quantity += r.Quantity; 
			} 
			return acc; 
		}, {});
		this._resources = Object.getOwnPropertyNames(resources).map(function(prop) {
			return resources[prop];
		});
	}
}

export default Mission;