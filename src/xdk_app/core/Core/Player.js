class Player {
	
	constructor(role, robot, resources) {
		this._role = role;
		this._robot = robot;
		this._resources = resources || [];
		this._index = -1;
		this._bid = [];
	}

	get Role() {
		return this._role;
	}

	get Robot() {
		return this._robot;
	}

	get Resources() {
		return this._resources;
	}

	get TotalResources() {
		return this._resources.reduce(function(acc, r) { 
			return acc + r.Quantity; 
		}, 0);
	}

	get Bid() {
		return this._bid;
	}

	get Index() {
		return this._index;
	}

	set Index(value) {
		this._index = value;
	}
}

export default Player;