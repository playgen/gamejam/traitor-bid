(function() {
	Array.prototype.shuffle = function() { 
		return this.sort(function(n) { 
			return Math.random() > 0.5 ? -1 : 1; 
		});
	}
})();

import GameConfig from 'GameConfig';
import Game from 'Game';
import Resource from 'Resource';

window.GameConfig = GameConfig;
window.Game = Game;
window.Resource = Resource;