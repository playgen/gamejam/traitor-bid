var gulp = require("gulp");

var angularTemplateCache = require("gulp-angular-templatecache");
var angularFileSort = require("gulp-angular-filesort");
var concat = require("gulp-concat");
var cleanCSS = require("gulp-clean-css");
var cssUseref = require("gulp-css-useref");
var del = require("del");
var eventStream = require("event-stream");
var filter = require("gulp-filter");
var gulpif = require("gulp-if");
var hash = require("gulp-hash-filename");
var inject = require("gulp-inject");
var jshint = require("gulp-jshint");
var rename = require("gulp-rename");
var sourcemaps = require("gulp-sourcemaps");
var uglify = require("gulp-uglify");
var webpackStream = require('webpack-stream');
var webpack2 = require('webpack');

/*============================================
PROJECT CONFIGURATION   
============================================*/
var config = {
	index: "index.html",	
	app: {
		root: "ng-app",
		scripts: "ng-app/**/*.js",
		styles: "ng-app/**/*.css",
		templates: "ng-app/**/*.html",
		images: "ng-app/**/*.png",
	},
	core: {
		entry: "core/app.js",
		scripts: [
			"core/**/*.js",
		]
	},
	xdk: {
		base: ".",
		scripts: [
			"xdk/**/*.js",
		],
		styles: [
			"xdk/**/*.css",
		],
	},
	dev: {
		vendor: {
			scripts: [
				"node_modules/jquery/dist/jquery.js",
				"node_modules/angular/angular.js",
				"node_modules/angular-ui-router/release/angular-ui-router.js",
				"node_modules/angular-animate/angular-animate.js",
				"node_modules/angular-aria/angular-aria.js",
				"node_modules/hammerjs/hammer.js",
				"node_modules/angular-hammer/angular.hammer.js",
			],
			styles: [
			]
		},
		sourcemaps: true,
		bundle: false,
		minifyScripts: false,
		minifyCss: false,
		hash: false,
		output: {
			app: "www/app",
			vendor: "www/vendor",
			xdk: "www/xdk",
		}		
	},
	build: {
		hashFormat: "{name}.{hash}.{ext}",
		output: "www"
	},
	devTools: {
		esversion: 6
	}
};

/*============================================
ACTIVE CONFIG
============================================*/
var activeConfig = null

function setDev(done) {
	activeConfig = config.dev;
	done();
}

function setProd(done) {
	activeConfig = config.prod;
	done();
}

/*============================================
STREAMS
============================================*/
function clean() {
 	return del([config.build.output + "/*"], { force: true });
}

function validateJs() {
	return gulp.src(config.app.scripts)
		.pipe(jshint({esversion: config.devTools.esversion}))
		.pipe(jshint.reporter("jshint-stylish"));
};

function appScripts() {
	return gulp.src(config.app.scripts)		
		.pipe(gulpif(activeConfig.sourcemaps, sourcemaps.init()));
};

function coreBabelWebpack() {
	return gulp.src(config.core.scripts)
		.pipe(webpackStream(require('./webpack.config.js'), webpack2));
}

function xdkScripts() {
	return gulp.src(config.xdk.scripts)		
		.pipe(gulpif(activeConfig.sourcemaps, sourcemaps.init()));
};

// combine the angular template html files into one javascript blob
function templates() {
	return gulp.src(config.app.templates)
		.pipe(angularTemplateCache({standalone: true}))
};

/*============================================
OUTPUT STREAMS
============================================*/
function generateIndex() {
	console.log("Generating index");
	return modifyIndex(config.index, { all: true });
}

function updateIndex(sections) {
	console.log("Updating index: ");
	return modifyIndex(config.build.output + "/index.html", sections);
}

function modifyIndex(index, sections) {
	return gulp.src(index)		
		// .pipe(gulpif(sections.all || sections.vendorStyles, inject(vendorStylesOutput(), {name: "vendor", ignorePath: config.build.output})))
		.pipe(gulpif(sections.all || sections.vendorScripts, inject(vendorScriptsOutput(), {
			name: "vendor", 
			ignorePath: config.build.output,
			relative: true,
		})))
		.pipe(gulpif(sections.all || sections.appStyles, inject(appStylesOutput(), {
			name: "app", 
			ignorePath: config.build.output,
			relative: true,
		})))				
		.pipe(gulpif(sections.all || sections.appScripts, inject(appScriptsOutput(), {
			name: "app", 
			ignorePath: config.build.output,
			relative: true,
		})))
		.pipe(gulpif(sections.all || sections.xdkStyles, inject(xdkStylesOutput(), {
			name: "xdk", 
			ignorePath: config.build.output,
			relative: true,
		})))
		.pipe(gulpif(sections.all || sections.xdkScripts, inject(xdkScriptsOutput(), {
			name: "xdk", 
			ignorePath: config.build.output,
			relative: true,
		})))
		.pipe(gulp.dest(config.build.output));
}

function allStylesOutput() {
	return eventStream.merge(
		vendorStylesOutput(),
		appStylesOutput()
	);
}

function appScriptsOutput() {
	return eventStream.merge(
			eventStream.merge(
				appScripts(),
				templates()
			)				
			.pipe(angularFileSort())		// puts files in correct order to satisfy angular dependency injection		
			.pipe(gulpif(activeConfig.bundle, concat("app.js")))				
			.pipe(gulpif(activeConfig.hash, hash({"format": config.build.hashFormat})))	
			.pipe(gulpif(activeConfig.bundle, rename(path => path.basename += ".bundle")))	
			.pipe(gulpif(activeConfig.minifyScripts, uglify()))
			.pipe(gulpif(activeConfig.minifyScripts, rename(path => path.basename += ".min")))
			.pipe(gulpif(activeConfig.sourcemaps, sourcemaps.write('.'))),

			coreBabelWebpack().pipe(gulpif(activeConfig.sourcemaps, sourcemaps.write('.')))
		)		
		.pipe(gulp.dest(activeConfig.output.app));
}

function appStylesOutput() {
	var cssFilter = filter("**/*.css", {restore: true});

	return gulp.src(config.app.styles)						
		.pipe(cssUseref({base: "assets"}))	// copies referenced files (fonts/images) within the css file		
		
		// filter to apply transformations only to .css files
		.pipe(cssFilter)
		.pipe(gulpif(activeConfig.bundle, concat("app.css")))		
		.pipe(gulpif(activeConfig.hash, hash({"format": config.build.hashFormat})))	
		.pipe(gulpif(activeConfig.bundle, rename(path => path.basename += ".bundle")))	
		.pipe(gulpif(activeConfig.minifyCss, cleanCSS()))
		.pipe(gulpif(activeConfig.minifyCss, rename(path => path.basename += ".min")))
		.pipe(cssFilter.restore)

		.pipe(gulp.dest(activeConfig.output.app));
};

function xdkScriptsOutput() {
	return xdkScripts()			
		.pipe(gulpif(activeConfig.bundle, concat("xdk.js")))				
		.pipe(gulpif(activeConfig.hash, hash({"format": config.build.hashFormat})))	
		.pipe(gulpif(activeConfig.bundle, rename(path => path.basename += ".bundle")))	
		.pipe(gulpif(activeConfig.minifyScripts, uglify()))
		.pipe(gulpif(activeConfig.minifyScripts, rename(path => path.basename += ".min")))
		.pipe(gulpif(activeConfig.sourcemaps, sourcemaps.write('.')))	// write sourcemaps for processed files
		
		.pipe(gulp.dest(activeConfig.output.xdk));
}

function xdkStylesOutput() {
	var cssFilter = filter("**/*.css", {restore: true});

	return gulp.src(config.xdk.styles)						
		// filter to apply transformations only to .css files
		.pipe(cssFilter)
		.pipe(gulpif(activeConfig.bundle, concat("xdk.css")))		
		.pipe(gulpif(activeConfig.hash, hash({"format": config.build.hashFormat})))	
		.pipe(gulpif(activeConfig.bundle, rename(path => path.basename += ".bundle")))	
		.pipe(gulpif(activeConfig.minifyCss, cleanCSS()))
		.pipe(gulpif(activeConfig.minifyCss, rename(path => path.basename += ".min")))
		.pipe(cssFilter.restore)

		.pipe(gulp.dest(activeConfig.output.xdk));
};

function vendorScriptsOutput() {
	return gulp.src(activeConfig.vendor.scripts)				
		.pipe(gulpif(activeConfig.bundle, concat("vendor.js")))
		.pipe(gulpif(activeConfig.hash, hash({"format": config.build.hashFormat})))	
		.pipe(gulpif(activeConfig.bundle, rename(path => path.basename += ".bundle")))	
		.pipe(gulpif(activeConfig.minifyScripts, uglify()))
		.pipe(gulpif(activeConfig.minifyScripts, rename(path => path.basename += ".min")))
		.pipe(gulp.dest(activeConfig.output.vendor));
}

function vendorStylesOutput() {
	var cssFilter = filter("**/*.css", {restore: true});

	return gulp.src(activeConfig.vendor.styles)		
		.pipe(cssUseref({base: "assets"}))	// copies referenced files (fonts/images) within the css file		

		// filter to apply transformations only to .css files
		.pipe(cssFilter)	
		.pipe(gulpif(activeConfig.bundle, concat("vendor.css")))	
		.pipe(gulpif(activeConfig.hash, hash({"format": config.build.hashFormat})))			
		.pipe(gulpif(activeConfig.bundle, rename(path => path.basename += ".bundle")))	
		.pipe(gulpif(activeConfig.minifyCss, cleanCSS()))	
		.pipe(gulpif(activeConfig.minifyCss, rename(path => path.basename += ".min")))	
		.pipe(cssFilter.restore)	

		.pipe(gulp.dest(activeConfig.output.vendor));
};

// function unityContent() {
// 	return gulp.src(config.unity.content, { base: config.unity.base })
// 		.pipe(gulp.dest(activeConfig.output.unity));
// }

/*============================================
TASKS
============================================*/
gulp.task(clean);

gulp.task(validateJs);

gulp.task("build-dev", gulp.series(clean, setDev, validateJs, generateIndex));

gulp.task("build-prod", gulp.series(clean, setProd, generateIndex));

gulp.task("watch-dev", function() {
	gulp.watch(config.index, gulp.series(setDev, "build-dev"));

	// gulp.watch(config.dev.vendor.styles, gulp.series(setDev, () => updateIndex({vendorStyles: true})));
	gulp.watch(config.dev.vendor.scripts, gulp.series("build-dev"));

	gulp.watch(config.core.scripts, gulp.series("build-dev"));

	gulp.watch(config.app.styles, gulp.series("build-dev"));
	gulp.watch(config.app.scripts, gulp.series("build-dev"));
	gulp.watch(config.app.templates, gulp.series("build-dev"));	
	gulp.watch(config.app.images, gulp.series("build-dev"));	
	
	gulp.watch(config.xdk.styles, gulp.series("build-dev"));
	gulp.watch(config.xdk.scripts, gulp.series("build-dev"));
});

gulp.task("default", gulp.series("build-dev", "watch-dev"));